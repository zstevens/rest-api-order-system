package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {

	private static final AtomicLong counter = Database.getCustomerCounter();
    private static Map<Long, Customer> customerDb = Database.getCustomerDb();
  
    /**
     * Create a new customer in the database
     * @param customer customer with first and last names
     * @return the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/customers/new")
    public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
    	customer.setNumber(counter.incrementAndGet());
    	customerDb.put(customer.getNumber(), customer);
    	return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a customer from the database
     * @param number the customer number
     * @return the customer if in the database, not found if not
     */
    @GetMapping("/customers/{number}")
    public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
    	if (customerDb.containsKey(number)) {
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	}
    }

    /**
     * Get a list of all customers from the database
     * @return the list of customers in the database
     */
    @GetMapping("/customers")
    public ResponseEntity<Object> getAllCustomers() {
        List<Customer> customers = new ArrayList<>(customerDb.values());
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    /**
     * Change a customer's first and last names
     * @param number the customer number
     * @return the new customer in the database
     */
    @PutMapping("/customers/edit/{number}")
    public ResponseEntity<Object> editCustomerName(@PathVariable long number, @RequestBody Customer customer) {
        if (customerDb.containsKey(number)) {
            customerDb.get(number).setFirstName(customer.getFirstName());
            customerDb.get(number).setLastName(customer.getLastName());
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get a customer's number given their first and last names
     * @param customer the first and last names of the customer to search for
     * @return the customer's number if in the database, HTTP not found if not
     */
    @GetMapping("/customers/search")
    public ResponseEntity<Object> searchCustomer(@RequestBody Customer customer) {
        String firstName = customer.getFirstName();
        String lastName = customer.getLastName();

        for (Map.Entry<Long, Customer> entry : customerDb.entrySet()) {
            Customer customerEntry = entry.getValue();
            if (customerEntry.getFirstName().equals(firstName) && customerEntry.getLastName().equals(lastName)) {
                return new ResponseEntity<>(customerEntry.getNumber(), HttpStatus.OK);
            }
        }

        return new ResponseEntity<>("Customer not found", HttpStatus.NOT_FOUND);
    }

}