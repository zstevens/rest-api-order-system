Start server with the command `./gradlew bootRun`

REST API endpoints:

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
POST | /customers/new | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Adds new customer to database
GET | /customers | | [ {"number": *number*, "firstName": "*firstName*", "lastName": "*lastName*"}, ... ] | Returns list of all customers
GET | /customers/*number* | | {"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | Returns customer with given number
PUT | /customers/edit/*number* | {"firstName": "*firstName*", "lastName": "*lastName*"} | {"number": *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | Edits the name of the customer with given number
GET | /customers/search | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Gets a customer's number by first and last name
POST | /products/new | {"price": *price*, "description": *description*} | *sku* | Adds new product to database
GET | /products/*sku* | | {"sku": *sku*, "price": *price*, "description": *description*} | Returns product with given number
POST | /orders/new/*customerNumber* | | *orderNumber* | Adds new order to database for this customer
GET | /orders/*number*/customer | | *customerNumber* | Returns customer for this order number
GET | /orders/*number*/lines | | [{"sku": *sku*, "quantity": *quantity*}, ...] | Returns array of order lines for this order
PUT | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Increase quantity of the product in this order
DELETE | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Decrease quantity of the product in this order
GET | /order/customer/*number* | | [*orderNumber*, ... ] | Gets a list of order numbers for all of a customer's orders
